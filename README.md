Getting started:

* install clickable: http://clickable.bhdouglass.com
* build dependency on the host (despite docker):
```
sudo apt-get install qemu-user-static
```
* clone:
```
git clone --recurse-submodules git@gitlab.com:dekkan/dekko.git
```
* connect your device via usb cable
* build and run:
```
cd dekko
clickable
```
